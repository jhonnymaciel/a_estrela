import math
import dists

# goal sempre sera 'bucharest'
def a_star(start, goal='Bucharest'):
    """
    Retorna uma lista com o caminho de start até 
    goal segundo o algoritmo A*
    """
    
    g = 0
    h = dists.straight_line_dists_from_bucharest[start]
    f = g + h
    
    cidade_inicio = {
                    'cidade': start,
                    'g': g,
                    'h': h,
                    'f': f,
                    'cidade_anterior': None,
                    'isFronteira': False
                    }
    
    percurso = [cidade_inicio]
    
    if start == goal:
        return percurso
        
    percurso = proxima_cidade(percurso, goal, cidade_inicio)
    
    nome_cidade_atual = goal
    caminho_percorrido = [goal]
    while nome_cidade_atual != start:
        for cidade in percurso:
            if cidade['cidade'] == nome_cidade_atual:
                nome_cidade_atual = cidade['cidade_anterior']
                caminho_percorrido.append(nome_cidade_atual)
        

    print("Percurso: ")
    caminho_percorrido.reverse()
    
    for c in caminho_percorrido:
        print(c+" >>")
    
    

def proxima_cidade(percurso, destino_final, cidade_atual):
    vizinhos = dists.dists[cidade_atual['cidade']]
    cidade_atual['isFronteira'] = False
    
    prox_cidade = None
    for v in vizinhos:
        if cidade_atual['cidade_anterior'] != v[0]:
            g = cidade_atual['g'] + v[1]
            h = dists.straight_line_dists_from_bucharest[v[0]]
            f = g + h
            cidade = {
                        'cidade': v[0],
                        'g': g,
                        'h': h,
                        'f': f,
                        'cidade_anterior': cidade_atual['cidade'],
                        'isFronteira' : True
                     }
                
            percurso = adiciona_cidade_percurso(percurso, cidade)
            
            if prox_cidade == None or cidade['f'] < prox_cidade['f']:
                prox_cidade = cidade
    
    print(cidade_atual)
    
    if prox_cidade['cidade'] != destino_final :
        
        for p in percurso:
            if p['f'] < prox_cidade['f'] and p['isFronteira'] == True:
                prox_cidade = p
        print(percurso)       
        return proxima_cidade(percurso, destino_final, prox_cidade)
    else:
        return percurso
    
    
def adiciona_cidade_percurso(percurso, cidade):
    print(cidade)
    print(percurso)
    c = [x for x in percurso if x['cidade'] == cidade['cidade']]
    print(c) 
    if len(c) > 0:
        if c[0]['f'] > cidade['f']:
            percurso.remove(c[0])
            percurso.append(cidade)
    else:
        percurso.append(cidade)
        
    return percurso
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    